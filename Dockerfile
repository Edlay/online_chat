FROM ruby:3.2.2

# Установка зависимостей
RUN apt-get update -qq && \
    apt-get install -y build-essential libpq-dev nodejs npm && \
    gem install bundler -v 2.1.4 && \
    npm install -g esbuild

# Создание директории приложения
WORKDIR /app

# Копирование всех файлов проекта
COPY . /app/

# Добавьте путь к исполняемым файлам Ruby в PATH
ENV PATH="/app/bin:${PATH}"

# Установка гемов и зависимостей
RUN npm install -g yarn && \
    bundle install && \
    bundle update

# Сборка проекта
RUN bundle exec rails db:create && \
    bundle exec rails db:migrate

EXPOSE 3008

# Запуск сервера Puma
CMD ["sh", "-c", "bundle exec rails db:migrate && bundle exec rails db:seed && yarn install && yarn build && bundle exec rails s -b 0.0.0.0 -p 3008"]

