# README

ruby - 3.2.2
ruby on rails - 7.1
port - 3008

Для сборки проекта используется - docker-compose build,
Для запуска проекта используется - docker-compose up,

Для сборки docker для тестов используется - docker-compose -f docker-compose.test.yml build,
Для запуска тестов используется - docker-compose -f docker-compose.test.yml run test.
