# frozen_string_literal: true

class ChatsController < ApplicationController
  def index
    @new_chat = Chat.new
    @chats = Chat.all
  end

  def create
    @new_chat = Chat.new(user: current_user)

    @new_chat.broadcast_append_to :chats if @new_chat&.save
  end

  def show
    @chat = Chat.find_by!(title: params[:title])
    @messages = @chat.messages
    @new_message = current_user&.messages&.build
  end
end
