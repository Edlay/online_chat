// Entry point for the build script in your package.json
//= require bootstrap
//= require turbo-rails
//= require bootstrap/dist/css/bootstrap.min.css
//= require bootstrap/dist/js/bootstrap.bundle.min
import "@hotwired/turbo-rails"
import "./controllers"
