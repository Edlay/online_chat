# frozen_string_literal: true

class Message < ApplicationRecord
  belongs_to :chat
  belongs_to :user

  scope :sorted, -> { order(:id) }
  validates :body, presence: true
end
