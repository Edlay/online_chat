# frozen_string_literal: true

class PrivateMessage < ApplicationRecord
  belongs_to :sender, class_name: 'User', foreign_key: 'sender_id'
  belongs_to :recipient, class_name: 'User', foreign_key: 'recipient_id'

  scope :sorted, -> { order(:id) }
  scope :user_messages, lambda { |current_user, user|
    where('(sender_id = ? AND recipient_id = ?) OR (sender_id = ? AND recipient_id = ?)',\
          current_user.id, user.id, user.id, current_user.id)
  }
  validates :body, presence: true
end
