# frozen_string_literal: true

class User < ApplicationRecord
  has_many :chats, dependent: :destroy
  has_many :messages, dependent: :destroy
  has_many :sent_messages, class_name: 'PrivateMessage', foreign_key: 'sender_id'
  has_many :received_messages, class_name: 'PrivateMessage', foreign_key: 'recipient_id'

  devise :database_authenticatable, :registerable,
         :recoverable, :validatable
end
