# frozen_string_literal: true

class CreateChats < ActiveRecord::Migration[7.0]
  def change
    create_table :chats do |t|
      t.belongs_to :user, null: false, foreing_key: true
      t.string :title, null: false, index: true

      t.timestamps
    end
  end
end
