# frozen_string_literal: true

user1 = User.find_or_create_by!(email: 'user1@admin.com') do |user|
  user.password = 'password123'
end

User.find_or_create_by!(email: 'user2@admin.com') do |user|
  user.password = 'password123'
end

User.find_or_create_by!(email: 'user3@admin.com') do |user|
  user.password = 'password123'
end

Chat.find_or_create_by!(user: user1)
