# language: en
Feature: User interactions with private messages

  Background:
    Given the following user exists:
      | email               | password     |
      | user1@admin.com     | password123  |

  Scenario: User can register an account and send a private message
    Given the user is on the registration page
    When the user fills in the registration form
    Then the user should be successfully registered

    When the user clicks on 'Private message'
    And the user clicks on 'user1@admin.com'
    And the user writes 'test message' in the 'Your text' field and click 'Send'
    When the user clicks on 'Sign out'

    When the user clicks on 'Sign in'
    And the second user fills in the login form
    And the second user clicks on 'Private message'
    And the second user clicks on 'user_test@admin.com'
    Then the second user should see the 'test message' on the screen