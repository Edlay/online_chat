# frozen_string_literal: true

def register_user(email, password)
  visit(new_user_registration_path)
  fill_in('Email', with: email)
  fill_in('Password', with: password)
  click_button('Sign up')
end

def login_user(email, password)
  visit(new_user_session_path)
  fill_in('Email', with: email)
  fill_in('Password', with: password)
  click_button('Sign in')
end

Given('the following user exists:') do |table|
  table.hashes.each do |user_params|
    User.create!(user_params)
  end
end

Given('the user is on the registration page') do
  visit(new_user_registration_path)
end

When('the user fills in the registration form') do
  register_user('user_test@admin.com', 'password123')
end

Then('the user should be successfully registered') do
  expect(page).to have_content('Private message')
end

When("the user clicks on 'Private message'") do
  click_on('Private message')
end

When("the user clicks on 'user1@admin.com'") do
  click_link('user1@admin.com')
end

When('the user writes {string} in the {string} field and click {string}') do |message, field, button_text|
  fill_in(field, with: message)
  click_on(button_text)
  visit(current_path)
end

When("the user clicks on 'Sign out'") do
  click_on('Sign out')
end

When("the user clicks on 'Sign in'") do
  click_on('Sign in')
end

When('the second user fills in the login form') do
  login_user('user1@admin.com', 'password123')
end

When("the second user clicks on 'Private message'") do
  click_on('Private message')
end

When("the second user clicks on 'user_test@admin.com'") do
  click_link('user_test@admin.com')
end

Then("the second user should see the 'test message' on the screen") do
  expect(page).to have_content('test message')
end