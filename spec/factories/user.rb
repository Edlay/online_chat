# frozen_string_literal: true

FactoryGirl.define do
  factory :user do
    email 'user@test.com'
    password 'secret123'
    password_confirmation { password }
  end
end
