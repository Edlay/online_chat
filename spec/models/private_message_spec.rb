# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PrivateMessage, type: :model do
  let(:sender) { User.create(email: 'sender@example.com', password: 'password123') }
  let(:recipient) { User.create(email: 'recipient@example.com', password: 'password456') }
  let(:private_message1) { described_class.create(body: 'Hello', sender: sender, recipient: recipient) }
  let(:private_message2) { described_class.create(body: 'Hi', sender: sender, recipient: recipient) }

  it 'relation sender and recipient' do
    expect(private_message1.sender).to eq(sender)
    expect(private_message1.recipient).to eq(recipient)
  end

  it 'sorted by id' do
    expect(PrivateMessage.sorted).to eq([private_message1, private_message2])
  end

  it 'requires a message text' do
    private_message = PrivateMessage.create(sender: sender, recipient: recipient)

    expect(private_message.errors[:body]).to include("can't be blank")
  end

  describe '.user_messages' do
    before do
      3.times { described_class.create(sender: sender, recipient: recipient) }
      2.times { described_class.create(sender: recipient, recipient: sender) }
      4.times { described_class.create }
    end

    it 'returns messages between current_user and another user' do
      expect(described_class.user_messages(sender, recipient)).to eq(
        described_class.where(
          '(sender_id = ? AND recipient_id = ?) OR (sender_id = ? AND recipient_id = ?)',
          sender.id, recipient.id, recipient.id, sender.id
        )
      )
    end

    it 'does not return messages between other users' do
      expect(described_class.user_messages(sender, recipient)).not_to include(
        Message.where.not(
          '(sender_id = ? AND recipient_id = ?) OR (sender_id = ? AND recipient_id = ?)',
          sender.id, recipient.id, recipient.id, sender.id
        )
      )
    end
  end
end
